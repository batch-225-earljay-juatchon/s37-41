const express = require("express");
const router = express.Router();
const auth = require("../auth")

const courseController = require("../controllers/courseController");

//  Router for creating a course

router.post("/addCourse", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(result => res.send(result));
})

//  Route for updating the course

router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});


//  ACTIVITY

//  Route function that retrievinh all the courses - Admin

router.get("/getCourse", (req, res) => {
	courseController.getCourse().then(result => res.send(result));

});


//  Routes for archiving

//  Using Params
router.put("/archive/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(result => res.send(result));
});


//  Routes for unarchiving
router.put("/unarchive/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(result => res.send(result));
});

//  Router for Users in getting courses - Users only

router.get("/getAllActive", auth.verify, (req, res) => {

	courseController.getAllActive().then(result => res.send(result));
});
/*
	Plan for Capstone:

	Admin - Manage - (Product - All or limited - Active or Inactive) Ans: Both

	Users - Bumili - (Product - All or Limited - Active or Inactive) Ans: 

*/ 


module.exports = router;